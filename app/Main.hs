{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Main where

import           Data.GI.Base
import           GHC.Int
import qualified GI.Gtk                        as Gtk
import           System.Console.Docopt
import           System.Environment             ( getArgs )
import           Turtle

patterns :: Docopt
patterns = [docoptFile|USAGE.txt|]

getArgOrExit = getArgOrExitWith patterns

main :: IO ()
main = do
  args <- parseArgsOrExit patterns =<< getArgs

  let w = read $ getArgWithDefault args "512" (longOption "width") :: Int32
  let h = read $ getArgWithDefault args "384" (longOption "height") :: Int32

  Gtk.init Nothing
  win <- new Gtk.Window
             [#title := "Hi there", #defaultWidth := w, #defaultHeight := h]
  on win #destroy Gtk.mainQuit

  button <- new Gtk.Button [#label := "Click me"]
  on button
     #clicked
     (set button [#sensitive := False, #label := "Thanks for clicking me"])

  #add win button
  #showAll win
  Gtk.main
