# docopt-gtk-template

An application template for Gtk. This uses the gtk docker from
[ZenHaskell](http://zenhaskell.gitlab.io) to allow
you to rapidly prototype gtk applications.

## Building

Build with

    stack build

Run with

    stack exe -- app-exe

Edit the USAGE.txt to change command line options and they will be available in the code.
